# SPDX-FileCopyrightText: 2021 Muri Nicanor
# SPDX-License-Identifier: GPL-3.0-or-later

from itertools import chain
from errbot import BotPlugin, botcmd, arg_botcmd, re_botcmd
from bs4 import BeautifulSoup
import time
import re
import urllib
import collections
import ssl


class PageNotFoundError(Exception):
    pass


class Tails(BotPlugin):
    """
    Tails Ticket Functionality
    """
    DEFAULT_CACHESIZE = 5
    DEFAULT_DEDUPTIME = 180
    #DEFAULT_DEDUPTIME = 1
    CONFIG_TEMPLATE = {'ROOMS': []}

    PARSEARRAY = {
        'Debian bug': {
            'debian#([0-9]{3,6})': 'https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=%s',
            '(https://bugs.debian.org/cgi-bin/bugreport.cgi\?bug=[0-9]{3,7})': '%s',
            },
        'Tor ticket': {
            'tor#([0-9]{3,6})': 'https://trac.torproject.org/projects/tor/ticket/%s',
            '(https://trac.torproject.org/projects/tor/ticket/[0-9]{3,5})': '%s',
        },
        'MAT2 issue': {
            'mat#([0-9]{3,6})': 'https://0xacab.org/jvoisin/mat2/-/issues/%s',
        },
        'Tails MR': {
            '(^| |tails|puppet-tails)!([0-9]{2,6})': 'https://gitlab.tails.boum.org/tails/%s/-/merge_requests/%s',
            '(https://gitlab.tails.boum.org/tails/[a-z-]+/-/merge_requests/[0-9]{3,6})': '%s',
        },
        'Tails issue': {
            '(^| |tails|sysadmin)#([0-9]{2,6})': 'https://gitlab.tails.boum.org/tails/%s/-/issues/%s',
            '(https://gitlab.tails.boum.org/tails/[a-z-]+/-/issues/[0-9]{3,6})': '%s',
        },
    }

    def activate(self):
        super().activate()

    def get_configuration_template(self):
        """
        Defines the configuration structure this plugin supports
        """
        return self.CONFIG_TEMPLATE


    def check_configuration(self, configuration):
        """
        Triggers when the configuration is checked, shortly before activation
        Raise a errbot.utils.ValidationException in case of an error
        """
        pass

    def configure(self, configuration):
       if configuration is not None and configuration != {}:
          config = dict(chain(self.CONFIG_TEMPLATE.items(), configuration.items()))
       else:
          config = self.CONFIG_TEMPLATE
       super().configure(config)

    def callback_message(self, message):
        """
        Triggered for every received message that isn't coming from the bot itself
        """
        config = self.config or self.CONFIG_TEMPLATE
        if str(message.to) not in config['ROOMS']:
            self.log.info("Room is not in the ROOMS of the Tails plugin.")
            return
        if message.is_direct:
            return
        self.parse(message)

    def callback_botmessage(self, message):
        """
        Triggered for every message that comes from the bot itself

        You should delete it if you're not using it to override any default behaviour
        """
        pass

    def pushtourlcache(self, url, title):
        urlcache = self.get('URLCACHE', collections.OrderedDict())

        if len(urlcache) >= self.DEFAULT_CACHESIZE:
            urlcache.popitem(False)

        #urlcache[url] = str(title)
        self['URLCACHE'] = urlcache
        self.log.info("URLCACHE: {}".format(self['URLCACHE']))

    def pushtoroomhist(self, url, room):
        roomhist = self.get('ROOMHIST', {})

        roomhist[room] = collections.OrderedDict()
        if len(roomhist[room]) >= self.DEFAULT_CACHESIZE:
            roomhist[room].popitem(False)

        roomhist[room][url] = time.time()
        self['ROOMHIST'] = roomhist
        self.log.info("ROOMHIST: {}".format(self['ROOMHIST']))


    def parse(self, message):
        """
        we are using msg.to, because we can be sure it is a group message
        """
        self.log.info("Parse message from "+message.frm.node)
        room = message.frm.node + '@' + message.frm.domain
        for label, regexps in self.PARSEARRAY.items():
            for regexp, template in regexps.items():
                matches = re.findall(regexp, message.body)
                for match in matches:
                    # default to 'tails' when first match group is empty
                    if not match[0].strip():
                        match = ('tails',) + match[1:]

                    url = template % match
                    urlcache = self.get('URLCACHE', collections.OrderedDict())
                    title = self.gettitlefromhtml(url)
                    title = urlcache.get(url, title)
                    self.pushtourlcache(url, title)
        
                    roomhist = self.get('ROOMHIST', {})
        
                    if room in roomhist and roomhist[room].get(url):
                        if (time.time() - roomhist[room][url]) > self.DEFAULT_DEDUPTIME:
                            self.send(message.to, "%s:\n  %s\n  %s" % (label, title, url))
                            self.pushtoroomhist(url, room)
                    else:
                        self.send(message.to, "%s:\n  %s\n  %s" % (label, title, url))
                        self.pushtoroomhist(url, room)

    def gettitlefromhtml(self, url):
        self.log.info("Get title from this {}".format(url))
        sslcontext = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS)
        #sslcontext = ssl.create_default_context()
        self.log.info("SSLContext: {}".format(sslcontext.protocol))
        try:
            #response = urllib.request.urlopen(url)
            response = urllib.request.urlopen(url, context=sslcontext)
        except urllib.error.HTTPError as e:
            return "{}".format(e)

        data = response.read()

        b = BeautifulSoup(data, 'html.parser')
        title = b.find('title').contents[0]
        return self.fixtitle(title)

    def fixtitle(self, title):
        title = re.sub( '\s+', ' ', title ).strip()
        title = title.replace(" - Tails - Tails Ticket Tracker", "")
        title = title.replace("- Debian Bug report logs", "")
        title = title.replace(" Tor Bug Tracker & Wiki ", "")
        title = re.sub("\) ·.*$", ')', title)
        return title

    @botcmd
    def tails_list(self, message, args):
        """ list the rooms """
        self.send(message.frm, "%s" %(self.config))

    @botcmd
    def tails_add(self, message, args):
        """ add a room to this plugin """
        config = self.config or self.CONFIG_TEMPLATE
        config['ROOMS'].append(args)
        self.configure(config)
        self._bot.plugin_manager.set_plugin_configuration(
                self.name,
                getattr(self, 'config', self.CONFIG_TEMPLATE)
        )

    @botcmd
    def tails_del(self, message, args):
        """ remove a room from this plugin """
        config = self.config or self.CONFIG_TEMPLATE
        config['ROOMS'].remove(args)
        self.configure(config)
        self._bot.plugin_manager.set_plugin_configuration(
                self.name,
                getattr(self, 'config', self.CONFIG_TEMPLATE)
        )
